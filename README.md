# WordCount

This setups our development environment. It works on Linux or Mac. For PC, some steps may require some changes.

##Preconditions
+ Installed the following in your systems
    * [Java JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    * [Maven](https://maven.apache.org/)
    * git client (For PC, you can download and install TortoiseGit)
    * HDP 2.5 sandbox (I used the image for VirtualBox.)

+ Run HDP the first time and reset password. Start the sandbox. When it is up, log into it using the password *hadoop*. It would immediately prompt you to change password. Type *hadoop* as the current/old password. Then type in the new password two times.
```
ssh root@127.0.0.1
```
* Also update Ambari password to the same password by running
```
ambari-admin-password-reset
```

##How to Build
* Clone this repo. Update the \<username\> below to your bitbucket username.

```
git clone -b master https://<username>@bitbucket.org/cs410dso/wordcount.git
```
* Build using Maven
```
cd wordcount; mvn clean package
```
When build is successful, you will see wordcount-1.0.jar in target folder.

##How to Run
* Copy the jar file into sandbox.
```
scp -P 2222 target/wordcount-1.0.jar root@127.0.0.1:/root
```
* Download a sample text file and copy it into HDFS in sandbox
```
ssh root@127.0.0.1
wget https://s3.amazonaws.com/divvy-data/tripdata/Divvy_Trips_2016_Q1Q2.zip
unzip Divvy_Trips_2016_Q1Q2.zip
hadoop fs -mkdir /cs410
hadoop fs -mkdir /cs410/input
hadoop fs -put Divvy_Trips_2016_Q1Q2 /cs410/input
```

* Run the job
```
hadoop jar wordcount-1.0.jar /cs410/input /cs410/output
```

* See the result
Once the job is complete, you can see the results at hdfs://cs410/output
```
hadoop fs -cat /cs410/output/part*
```