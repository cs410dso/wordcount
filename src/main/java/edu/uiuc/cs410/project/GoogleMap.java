package edu.uiuc.cs410.project;

public class GoogleMap {

	public static void main(String[] args) 
	  {
		Station station1 = new Station() ;
		station1.setLatitude(41.866095);
		station1.setLongitude(-87.663897);

		Station station2 = new Station() ;
		station2.setLatitude(41.85381);
		station2.setLongitude(-87.665897);
				
		System.out.println(generateGoogleMapImageUrl(station1,station2));

	  }

	private static String generateGoogleMapImageUrl(Station station1, Station station2) {	
		StringBuilder url = new StringBuilder();
		url.append("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyA6BbcoJNd8o5r6FGrAVBLwpEvGUqGB25g");
		
		//Size of map
		url.append("&size=600x300");
		
		//Marker
		url.append("&markers=color:red%7Clabel:A%7C"+station1.getLatitude()+","+station1.getLongitude());
		url.append("&markers=color:blue%7Clabel:B%7C"+station2.getLatitude()+","+station2.getLongitude());

		//Path (add straight line from label A to B)
		url.append("&path="+station1.getLatitude()+","+station1.getLongitude()+"%7C"+station2.getLatitude()+","+station2.getLongitude());
		
		return url.toString();
		
	}


}
